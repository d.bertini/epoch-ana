#include <TString.h>

#include <iostream>
#include "mpi.h"

// C++
#include "cxxopts.hpp"
#include <chrono>
#include <iostream>
#include <random>
#include <string>
#include <sys/types.h>
#include <thread>
#include <unistd.h>

// ROOT
#include "TError.h"
#include "TFile.h"
#include "TH1D.h"
#include "TMPIFile2.h"
#include "TMemFile.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TSystem.h"
#include "TTree.h"

// Converter
#include "RunMode.h"
#include "TConverter.h"
#include "TSDFReader.h"

using std::cout;
using std::endl;

void PrintUsage()
{
  cout << "Usage\n"
       << "sdf2root [option] input file name\n"
       << "-a: all information (mesh and macro particle)\n"
       << "-m: all mesh information\n"
       << "-p: all macro particle\n"
       << endl;
}


#ifndef __CINT__
int main(int argc, char **argv)
{

  auto start = std::chrono::high_resolution_clock::now();
  TString inputName{""};
  TString outputName{""};
  RunMode runMode = RunMode::Interactive;

  // check arguments
  for (int i = 1; i < argc; i++) {
    cout << argv[i] << endl;
    TString arg{argv[i]};
    if (arg == "-h") {
      PrintUsage();
      return 0;
    } else if (arg.Contains(".sdf")) {
      inputName = arg;
      outputName = arg(arg.Last(TString("/")[0]) + 1,
                       arg.Index(".sdf") - (arg.Last(TString("/")[0]) +
                                            1));  // can you beliebe this shit?
      cout << inputName << endl;
      cout << outputName << endl;
    } else if (arg == "-a")
      runMode = RunMode::AllInfo;
    else if (arg == "-p")
      runMode = RunMode::AllParticles;
    else if (arg == "-m")
      runMode = RunMode::AllMeshes;
    else {
      PrintUsage();
      return 0;
    }
  }

  if (inputName == "") {
    PrintUsage();
    return 0;
  }

  // Reading file information
  TSDFReader *reader = new TSDFReader(inputName);

  Int_t counter = 0;
  for (auto block : reader->fBlock) {
    cout << counter++ << "\t" << block->GetBlockType() << "\t"
         << block->GetDataType() << "\t" << block->GetName() << "\t"
         << block->GetID() << "\t" << endl;
  }

  TConverter *converter = new TConverter(reader, outputName, runMode);
  converter->GetData();
  delete converter;
  delete reader;

  auto end = std::chrono::high_resolution_clock::now();
  double time =
      std::chrono::duration_cast<std::chrono::duration<double>>(end - start)
          .count();
  std::string msg = "Total elapsed time: ";
  msg += std::to_string(time);

  Info("sdf2root convertion: ", msg.c_str());

  
  return 0;
}

#endif
