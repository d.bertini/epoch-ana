#ifndef TCONVERTER
#define TCONVERTER 1

/*
    Converter class
*/

#include <vector>

#include <TFile.h>
#include <TTree.h>

#include "RunMode.h"
#include "TBlockPlainMesh.h"
#include "TMacroParticle.h"
#include "TMeshValue.h"
#include "TSDFReader.h"

class TConverter
{
 public:
  TConverter(TSDFReader *reader, TString outName, RunMode runMode);
  virtual ~TConverter();

  void GetData();

 private:
  TSDFReader *fReader;

  TString fOutName;

  RunMode fRunMode;

  // Particle values
  std::vector<TString> fParName;
  void FindPar();

  // For probe
  std::vector<TString> fProbeName;
  void FindProbe();

    ClassDef(TConverter, 1) 
};

#endif
