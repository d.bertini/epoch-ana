#ifndef RunMode_h
#define RunMode_h 1

enum class RunMode{
   Interactive,
   AllInfo,
   AllMeshes,
   AllParticles
};

#endif
